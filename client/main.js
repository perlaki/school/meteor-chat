import {
  Template
} from 'meteor/templating';
import {
  Count,
  Posts
} from '../collections';

import './main.html';

Template.body.onCreated(function bodyOnCreated() {
  Meteor.subscribe('posts');
  Meteor.subscribe('count')
});


Template.body.helpers({
  currentUser() {
    return Meteor.user()
  }
})

Template.hello.helpers({
  counter() {
    return Count.findOne({}).count;
  }
});

Template.chat.helpers({
  postings() {
    return Posts.find().fetch();
  }
});

Template.hello.events({
  'click button'() {
    Meteor.call('count.increment');
  },
});

Template.chat.events({
  'click #sendMessage'() {
    let message = $("#message").val();
    $("#message").val('')
    if (message)
      Meteor.call('posts.new', message);
  }
});