import {
    Mongo
} from 'meteor/mongo';

const Count = new Mongo.Collection('count');
const Posts = new Mongo.Collection('posts');

if (Meteor.isServer) {
    Meteor.publish('count', function countPublication() {
        return Count.find({});
    });

    Meteor.publish('posts', function postsPublication() {
        return Posts.find({})
    })

    Meteor.methods({
        'count.increment'() {
            let mycount = Count.findOne({});
            Count.update({
                _id: mycount._id
            }, {
                count: mycount.count + 1
            });
        },
        'posts.new'(message) {
            let user = Meteor.user()
            Posts.insert({
                message: message,
                user: user.profile ? user.profile.name : user.emails[0].address
            });
        }
    })
}

export {
    Count,
    Posts
};