import { Meteor } from 'meteor/meteor';
import {Count,Posts} from '../collections';

Meteor.startup(() => {
  // code to run on server at startup
  if(!Count.findOne({}))
    Count.insert({count:0});
  
  if(!Posts.findOne({})){
    Posts.insert({message:"mäh"});
    Posts.insert({message:"müh"});
    Posts.insert({message:"möh"});
  }
});
